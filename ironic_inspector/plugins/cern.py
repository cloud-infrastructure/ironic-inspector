#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.from oslo_config import cfg

from oslo_config import cfg

from ironic_inspector.plugins import base
from ironic_inspector import utils


LOG = utils.getProcessingLogger(__name__)
CONF = cfg.CONF
CERN_CAPABILITIES = CONF.cern.inspected_capabilities


class CernDeviceDetection(base.ProcessingHook):
    """Processing hook for detecting CERN custom hardware properties."""

    def before_update(self, introspection_data, node_info, **kwargs):
        inventory = utils.get_inventory(introspection_data,
                                        node_info=node_info)
        caps = {}

        caps.update(self._detect_cern_capabilities(inventory,
                                                   node_info,
                                                   introspection_data))

        if caps:
            LOG.info('CERN capabilities: %s', caps, node_info=node_info,
                     data=introspection_data)
            node_info.update_capabilities(**caps)
        else:
            LOG.info('No CERN capabilities detected', node_info=node_info,
                     data=introspection_data)

    def _detect_cern_capabilities(self, inventory, node_info, data=None):
        result = {}

        for key in [x.strip() for x in CERN_CAPABILITIES.split(",")]:
            try:
                result.update({key: inventory[key]})
            except KeyError:
                LOG.warning('%s not detected', key,
                            data=data, node_info=node_info)
                # result.update({key: None})

        return result
